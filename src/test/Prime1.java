/*
==========================================================
Author	: 		R�mi Kaeffer
Description : 	http://www.spoj.com/problems/PRIME1/
==========================================================
STATUS : WA
 */
package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Prime1 {
	public static void main(String[] args) throws Exception {
		ArrayList <ArrayList<String>> finalRes=new ArrayList <ArrayList<String>>();
		int[] prime={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t=Integer.parseInt(br.readLine());
		for(int i=0; i<t ; i++) {
			finalRes.add(new ArrayList<String>());
			String [] limit=br.readLine().split(" ");
			int a=Integer.parseInt(limit[0]);
			int b=Integer.parseInt(limit[1]);
			if(a % 2 == 0 ) {
				a=a+1;
			}
			while(a <= b) {
				if(isPrime(a)) {
					finalRes.get(i).add(Integer.toString(a));
				}
				a=a+2;
			}
		}
		for(int i=0;i<finalRes.size();i++) {
			for(int j=0;j<finalRes.get(i).size();j++) {
				System.out.println(finalRes.get(i).get(j));
			}
			System.out.println("");
		}
	}
	public static boolean isPrime(int number){
        for(int i=2; i<=Math.sqrt(number)+1; i++){
           if(number%i == 0){
               return false; //number is divisible so its not prime
           }
        }
        return true; //number is prime now
    }

}
