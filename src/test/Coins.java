/*
==========================================================
Author	: 		R�mi Kaeffer
Description : 	http://www.spoj.com/problems/COINS/
==========================================================
STATUS : AC in 0.06s
 */
package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Coins {
	
	static Map<Integer, Long> a = new HashMap<Integer,Long>();

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line="";
		
		while((line=br.readLine()) != null) {
			System.out.println(Long.toString(memo_coins(Integer.parseInt(line))));
		}
	}
	
	private static long memo_coins (int n) {
		if(n==0) {
			return 0;
		}
		if(a.containsKey(n)) {
			return a.get(n);
		}
		long newValue = Math.max(n, memo_coins((int)(n/2)) + memo_coins((int)(n/3)) + memo_coins((int)(n/4)));
		a.put(n, newValue);
		return newValue;
	}
}
